class LeapYear {
    Boolean isLeapYear(int year) {
        return (isYearDivisibleBy(year, 400)
                || isYearDivisibleBy(year, 4) && !isYearDivisibleBy(year, 100));
    }

    private boolean isYearDivisibleBy(int year, int divisor) {
        return year % divisor == 0;
    }
}
