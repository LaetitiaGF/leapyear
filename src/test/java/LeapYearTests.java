import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LeapYearTests {

    LeapYear leapYear = new LeapYear();

    @Test
    public void test_isLeapYearShouldReturnFalseFor2019() {
        assertFalse(leapYear.isLeapYear(2019));
    }

    @Test
    public void test_LeapYearShouldReturnTrueForYearsDivisibleBy400() {
        assertTrue(leapYear.isLeapYear(2000));
    }

    @Test
    public void test_LeapYearShouldReturnFalseForYearsDivisibleBy100ButNotBy400() {
        assertFalse(leapYear.isLeapYear(1700));
    }

    @Test
    public void test_LeapYearShouldReturnTrueForYearsDivisibleBy4ButNotBy100() {
        assertTrue(leapYear.isLeapYear(2008));
    }

    @Test
    public void test_LeapYearShouldReturnFalseForYearsNotDivisibleBy4() {
        assertFalse(leapYear.isLeapYear(2018));
    }

    @Test
    public void test_LeapYearShouldReturnFalseFor1800() {
        assertFalse(leapYear.isLeapYear(1800));
    }

    @Test
    public void test_LeapYearShouldReturnFalseFor1900() {
        assertFalse(leapYear.isLeapYear(1900));
    }

    @Test
    public void test_LeapYearShouldReturnFalseFor2100() {
        assertFalse(leapYear.isLeapYear(2100));
    }

    @Test
    public void test_LeapYearShouldReturnFalseFor2017() {
        assertFalse(leapYear.isLeapYear(2017));
    }

    @Test
    public void test_LeapYearShouldReturnTrueFor2012() {
        assertTrue(leapYear.isLeapYear(2012));
    }
}
